<?php

class Sekil
{
    public $teref1;
    public $teref2;
    public $teref3;
    public $sahe;
    public function __construct($teref1, $teref2 = 0, $teref3 = 0)
    {
        $this->teref1 = $teref1;
        $this->teref2 = $teref2;
        $this->teref3 = $teref3;
    }
}
class Duzbucaq extends Sekil
{
    public function sahe()
    {
        return $this->sahe = $this->teref1 * $this->teref2;
    }
}
class Ucbucaq extends Sekil
{
    public function sahe()
    {
        return $this->sahe = $this->teref1 * $this->teref2 /2;
    }
}
class Kvadrat extends Sekil
{
    public function sahe()
    {
        return $this->sahe = $this->teref1 ** 2;
    }
}
$sekil = new Duzbucaq(10, 10);
echo $sekil->sahe();
